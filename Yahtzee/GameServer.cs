﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.IO;

namespace Yahtzee
{
	public class GameServer
	{
		TcpListener listener;
		Dictionary<TcpClient, LocalPlayer> clients;
		LocalRule lr;
		

		public GameServer(int port)
		{
			lr = new LocalRule();
			clients = new Dictionary<TcpClient, LocalPlayer>();
			listener = new TcpListener(System.Net.IPAddress.Any, port);
			listener.Start();
			Action<Task<TcpClient>> clientConnected = null ;
			clientConnected = (t) =>
			{
				if (t.IsCompleted)
				{
					setupClient(t.Result);
					listener.AcceptTcpClientAsync().ContinueWith(clientConnected);
				}
			};
			listener.AcceptTcpClientAsync().ContinueWith(clientConnected);
		}

		internal int getClientCount()
		{
			return clients.Count;
		}

		private void handleRequest(TcpClient c, string request)
		{
			var parts = request.Split(' ');
			var outStream = new StreamWriter(c.GetStream());
			switch (parts[0])
			{
				case "r_roll":
					{
						var roll = lr.Roll();
						roll.Wait();
						outStream.WriteLine(roll.Result);
						break;
					}
				case "r_score":
					{
						var rolls = new int[] {
							int.Parse(parts[1]),
							int.Parse(parts[2]),
							int.Parse(parts[3]),
							int.Parse(parts[4]),
							int.Parse(parts[5])
						};
						ScoreSlot slot = (ScoreSlot) Enum.Parse(typeof(ScoreSlot), parts[6]);
						var score = lr.Score(rolls, slot);
						outStream.WriteLine(score.Result);
						break;
					}
				case "p_roll":
					{
						LocalPlayer p = clients[c];
						try
						{
							p.Roll();
							p.GetRoll().Wait();
							outStream.WriteLine(String.Join(" ", p.GetRoll().Result));
						}
						catch (Exception e)
						{
							outStream.WriteLine(e.ToString());
						}
						break;
					}
				case "p_reroll":
					{
						LocalPlayer p = clients[c];
						try
						{
							var keep = new bool[] {
								parts[1] == "1", 
								parts[2] == "1",
								parts[3] == "1",
								parts[4] == "1",
								parts[5] == "1"
							};
							p.Reroll(keep);
							p.GetRoll().Wait();
							outStream.WriteLine(String.Join(" ", p.GetRoll().Result));
						}
						catch (Exception e)
						{
							outStream.WriteLine(e.ToString());
						}
						break;
					}
				case "p_mark":
					{
						LocalPlayer p = clients[c];
						try
						{
							ScoreSlot slot = (ScoreSlot)Enum.Parse(typeof(ScoreSlot), parts[1]);
							var slotScore = p.MarkScore(slot);
							slotScore.Wait();
							outStream.WriteLine(String.Join(" ", slotScore.Result));
						}
						catch (Exception e)
						{
							outStream.WriteLine(e.ToString());
						}
						break;
					}
				default:
					{
						outStream.WriteLine("invalid request");
						break;
					}
			}
			outStream.Flush();
		}

		private void setupClient(TcpClient client)
		{
			clients.Add(client, new LocalPlayer(lr));
			StreamReader sr = new StreamReader(client.GetStream());
			Action<Task<string>> messageRecieved = null;
			messageRecieved = (t) =>
			{
				if (t.IsCompleted)
				{
					try
					{
						handleRequest(client, t.Result);
						sr.ReadLineAsync().ContinueWith(messageRecieved);
					}
					catch (Exception)
					{
						client.Close();
						clients.Remove(client);
					}
				}
			};
			try
			{
				var rl = sr.ReadLineAsync();
				rl.ContinueWith(messageRecieved);
			}
			catch (Exception e)
			{
			}
		}
	}
}
