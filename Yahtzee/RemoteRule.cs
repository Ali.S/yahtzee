﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.IO;
namespace Yahtzee
{
	public class RemoteRule : IRules
	{
		TcpClient client;
		StreamReader reader;
		StreamWriter writer;
		public RemoteRule(TcpClient client)
		{
			this.client = client;
			if (!client.Connected)
				throw new ArgumentException("client is not connected");
			reader = new StreamReader(client.GetStream());
			writer = new StreamWriter(client.GetStream());
		}

		public Task<int> Roll()
		{
			writer.WriteLine("r_roll");
			writer.Flush();
			return reader.ReadLineAsync().ContinueWith((line) => int.Parse(line.Result));
		}

		public Task<int> Score(int[] roll, ScoreSlot slot)
		{
			writer.WriteLine("r_score " + String.Join(" ", roll)  + " " +slot.ToString());
			writer.Flush();
			return reader.ReadLineAsync().ContinueWith((line) => int.Parse(line.Result));
		}
	}
}
