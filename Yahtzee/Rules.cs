﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yahtzee
{
	public enum ScoreSlot
	{
		Sum_1,
		Sum_2,
		Sum_3,
		Sum_4,
		Sum_5,
		Sum_6,
		ThreeOfKind,
		FourOfKind,
		Straight_s,
		Straight_l,
		FullHouse,
		Yahtzee,
		Chance
	};

	public interface IRules
	{
		Task<int> Roll();
		Task<int> Score(int[] roll, ScoreSlot slot);
	}
}
