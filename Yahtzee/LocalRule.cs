﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yahtzee
{
	public class LocalRule : IRules
	{
		Random rng = new Random();

		virtual public Task<int> Roll()
		{

			return Task.Run(() => {
				return rng.Next() % 6 + 1;
				});
		}

		virtual public Task<int> Score(int[] roll, ScoreSlot slot)
		{
			return Task.Run(() => {

				if (roll.Length != 5)
					throw new ArgumentException("roll should have exactly 5 indices");
				for (int i = 0; i < 5; i++)
					if (roll[i] < 1 || roll[i] > 6)
						throw new ArgumentException("roll should only contain dice values");
				int upperSection = 0;
				switch (slot) {
					case ScoreSlot.Sum_1:
						upperSection = 1;
						break;
					case ScoreSlot.Sum_2:
						upperSection = 2;
						break;
					case ScoreSlot.Sum_3:
						upperSection = 3;
						break;
					case ScoreSlot.Sum_4:
						upperSection = 4;
						break;
					case ScoreSlot.Sum_5:
						upperSection = 5;
						break;
					case ScoreSlot.Sum_6:
						upperSection = 6;
						break;
				}
				if (upperSection != 0)
				{
					int sum = 0;
					for (var i = 0; i < 5; i++)
						if (roll[i] == upperSection)
							sum += roll[i];
					return sum;
				}
				int[] occurance = new int[] { 0, 0, 0, 0, 0, 0 };
				for (int i = 0; i < roll.Length; i++)
				{
					occurance[roll[i] - 1]++;
				}
				switch (slot) {
					case ScoreSlot.ThreeOfKind:
						if (occurance.Any((x) => x >= 3))
							return roll.Sum();
						else
							return 0;
					case ScoreSlot.FourOfKind:
						if (occurance.Any((x) => x >= 4))
							return roll.Sum();
						else
							return 0;
					case ScoreSlot.Straight_s:
						if ((occurance[0] > 0 && occurance[1] > 0 && occurance[2] > 0 && occurance[3] > 0) ||
							(occurance[1] > 0 && occurance[2] > 0 && occurance[3] > 0 && occurance[4] > 0) ||
							(occurance[2] > 0 && occurance[3] > 0 && occurance[4] > 0 && occurance[5] > 0))
							return 30;
						else
							return 0;
					case ScoreSlot.Straight_l:
						if ((occurance[0] > 0 && occurance[1] > 0 && occurance[2] > 0 && occurance[3] > 0 && occurance[4] > 0) ||
							(occurance[1] > 0 && occurance[2] > 0 && occurance[3] > 0 && occurance[4] > 0 && occurance[5] > 0))
							return 40;
						else
							return 0;
					case ScoreSlot.FullHouse:
						if ((occurance.Any((x) => x == 3) && occurance.Any((x) => x == 2)) || occurance.Any((x) => x == 5))
							return 25;
						else
							return 0;
					case ScoreSlot.Yahtzee:
						if (occurance.Any((x) => x == 5))
							return 50;
						else
							return 0;
					case ScoreSlot.Chance:
						return roll.Sum();
				}
				throw new ArgumentOutOfRangeException("Invalid ScoreSlot");
			});
		}
	}
}
