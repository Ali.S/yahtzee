using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;

namespace Yahtzee
{
	class Program
	{
		static void Main(string[] args)
		{
			const int gamePort = 25613;
			string line = " " ;
			while (line.ToLower()[0] != 's' && line.ToLower()[0] != 'c')
			{
				Console.WriteLine("(S)erver or (C)lient?");
				line = Console.ReadLine();
			}
			if (line.ToLower()[0] == 's')
			{
				GameServer server = new GameServer(gamePort);
				while (true)
				{
					Console.WriteLine("clients: " + server.getClientCount().ToString());
					System.Threading.Thread.Sleep(1000);
				}
			}

			Console.WriteLine("Server Address?");
			line = Console.ReadLine();
			TcpClient client = new TcpClient(line, gamePort);
			if (!client.Connected)
				Console.WriteLine("Server not available");

			RemotePlayer player = new RemotePlayer(client);
			HashSet<ScoreSlot> slots = new HashSet<ScoreSlot>();
			foreach (ScoreSlot s in Enum.GetValues(typeof(ScoreSlot)))
				slots.Add(s);
			for (var i = 0; i < 13; i++)
			{
				player.Roll();
				player.GetRoll().Wait();
				Console.WriteLine("Your roll is: " + String.Join(", ", player.GetRoll().Result));
				Console.WriteLine("Keep? (0|1) (0|1) (0|1) (0|1) (0|1)");
				line = "";
				while (parseReroll(line).Length != 5)
					line = Console.ReadLine();
				player.Reroll(parseReroll(line));
				player.GetRoll().Wait();
				Console.WriteLine("Your roll is: " + String.Join(", ", player.GetRoll().Result));
				Console.WriteLine("Keep? (0|1) (0|1) (0|1) (0|1) (0|1)");
				line = "";
				while (parseReroll(line).Length != 5)
					line = Console.ReadLine();
				player.Reroll(parseReroll(line));
				Console.WriteLine("Your roll is: " + String.Join(", ", player.GetRoll().Result));
				Console.WriteLine("Which slot? " + String.Join(" ", slots.ToArray()));
				line = "";
				do
				{
					line = Console.ReadLine();
					try
					{
						if (slots.Contains((ScoreSlot)Enum.Parse(typeof(ScoreSlot), line)))
							break;
					}
					catch (Exception)
					{
					}
					Console.WriteLine("Not a valid slot");
				} while (true);
				player.MarkScore((ScoreSlot)Enum.Parse(typeof(ScoreSlot), line));
				player.GetScore().Wait();
				Console.WriteLine("Total Score = " + player.GetScore().Result.ToString());
			}

		}

		static bool[] parseReroll(String line)
		{
			return line.Split(' ').Select((s) => s == "1" ? true : false).ToArray();
		}
	}
}
