﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.IO;

namespace Yahtzee
{
	public class RemotePlayer : IPlayer
	{
		TcpClient client;
		StreamReader reader;
		StreamWriter writer;
		Task<int[]> roll;
		Dictionary<ScoreSlot, Task<int>> recordedScores;
		int rerollCount;

		public RemotePlayer(TcpClient client)
		{
			this.client = client;
			if (!client.Connected)
				throw new ArgumentException("client is not connected");
			reader = new StreamReader(client.GetStream());
			writer = new StreamWriter(client.GetStream());
			roll = null;
			recordedScores = new Dictionary<ScoreSlot, Task<int>>();
		}

		public Task<int[]> GetRoll()
		{
			return roll;
		}

		public Task<int> GetScore()
		{
			return Task.WhenAll(recordedScores.Values).ContinueWith((s) => s.Result.Sum());
		}

		public Task<int> MarkScore(ScoreSlot slot)
		{
			if (roll == null)
				throw new Exception("player should start with a roll");
			roll.Wait();

			if (recordedScores.ContainsKey(slot) == false)
			{
				writer.WriteLine("p_mark " + slot.ToString());
				writer.Flush();
				recordedScores.Add(slot, reader.ReadLineAsync().ContinueWith((t) =>
				{
					
					try
					{
						return int.Parse(t.Result);
						
					}
					catch (Exception)
					{
						recordedScores.Remove(slot);
						throw new Exception(t.Result);
					}
				}));
				return recordedScores[slot];
			}
			else
				throw new Exception("Same slot can not be scored twice");

		}

		public void Reroll(bool[] keep)
		{
			if (roll == null)
				throw new Exception("player should start with a roll");
			if (rerollCount == 2)
				throw new Exception("At most two rerolls are possible");
			if (!roll.IsCompleted)
				roll.Wait();
			rerollCount++;
			writer.WriteLine("p_reroll " + String.Join(" ", keep.Select((b) => (b?"1" : "0"))));
			writer.Flush();
			roll = reader.ReadLineAsync().ContinueWith((t) =>
			{
				try
				{
					return t.Result.Split(' ').Select((d) => int.Parse(d)).ToArray();
				}
				catch (Exception)
				{
					throw new Exception(t.Result);
				}
			});
		}

		public void Roll()
		{
			GetScore().Wait();
			rerollCount = 0;
			writer.WriteLine("p_roll");
			writer.Flush();
			roll = reader.ReadLineAsync().ContinueWith((t) =>
			{
				try
				{
					return t.Result.Split(' ').Select((d) => int.Parse(d)).ToArray();
				}
				catch (Exception)
				{
					throw new Exception(t.Result);
				}
			});
		}
	}
}
