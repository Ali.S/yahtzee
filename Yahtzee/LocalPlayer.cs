﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yahtzee
{
	public class LocalPlayer : IPlayer
	{
		IRules gameMaster;
		Task<int[]> roll;

		Dictionary<ScoreSlot, Task<int>> recordedScores;
		int rerollCount;
		public LocalPlayer(IRules gameMaster)
		{
			this.gameMaster = gameMaster;
			recordedScores = new Dictionary<ScoreSlot, Task<int>>();
			roll = null;
		}
		public void Roll()
		{
			if (roll != null)
				throw new Exception("Multiple rolls without scoring");
			rerollCount = 0;
			Task<int>[] tasks = new Task<int>[] {
				gameMaster.Roll(),
				gameMaster.Roll(),
				gameMaster.Roll(),
				gameMaster.Roll(),
				gameMaster.Roll()
			};
			roll = Task.WhenAll(tasks);
		}
		public void Reroll(bool[] keep)
		{
			if (roll == null)
				throw new Exception("player should start with a roll");
			if (rerollCount == 2)
				throw new Exception("At most two rerolls are possible");
			rerollCount++;
			roll.Wait();
			
			Task<int>[] tasks = new Task<int>[5];
			var res = roll.Result;
			for (var i = 0; i < 5; i++)
			{
				if (keep[i])
				{
					int k = i;
					tasks[i] = Task.Run(() => res[k]);
				}
				else
					tasks[i] = gameMaster.Roll();
			}
			roll = Task.WhenAll(tasks);
		}
		public Task<int[]> GetRoll()
		{
			return roll;
		}

		public Task<int> MarkScore(ScoreSlot slot)
		{
			if (roll == null)
				throw new Exception("player should start with a roll");
			
			roll.Wait();
			
			if (recordedScores.ContainsKey(slot) == false)
			{
				if (recordedScores.ContainsKey(ScoreSlot.Yahtzee))
				{
					int[] occurance = new int[] { 0, 0, 0, 0, 0, 0 };
					for (int i = 0; i < roll.Result.Length; i++)
					{
						occurance[roll.Result[i] - 1]++;
					}
					if (occurance.Any((x) => x == 5))
					{
						ScoreSlot force = ScoreSlot.Chance;
						switch (roll.Result[0]) {
							case 1: force = ScoreSlot.Sum_1; break;
							case 2: force = ScoreSlot.Sum_2; break;
							case 3: force = ScoreSlot.Sum_3; break;
							case 4: force = ScoreSlot.Sum_4; break;
							case 5: force = ScoreSlot.Sum_5; break;
							case 6: force = ScoreSlot.Sum_6; break;
						}
						if (slot != force && !recordedScores.ContainsKey(force))
							throw new Exception("Corrisponding upper section should be filled first for second yahtzee");
						var tasks = new Task<int>[]{
							gameMaster.Score(roll.Result, slot),
							gameMaster.Score(roll.Result, ScoreSlot.Yahtzee)
						};
						recordedScores.Add(slot, Task.WhenAll(tasks).ContinueWith((s) => s.Result.Sum()));
					}
					else
						recordedScores.Add(slot, gameMaster.Score(roll.Result, slot));
				}
				else
					recordedScores.Add(slot, gameMaster.Score(roll.Result, slot));
			}
			else
				throw new Exception("Same slot can not be scored twice");

			roll = null;
			return recordedScores[slot];
		}

		public Task<int> GetScore()
		{
			return Task.WhenAll(recordedScores.Values).ContinueWith((s) => s.Result.Sum());
		}
	}
}

