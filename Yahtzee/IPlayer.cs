﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yahtzee
{
	interface IPlayer
	{
		void Roll();
		void Reroll(bool[] keep);
		Task<int> MarkScore(ScoreSlot slot);
		Task<int[]> GetRoll();
		Task<int> GetScore();
	}
}
