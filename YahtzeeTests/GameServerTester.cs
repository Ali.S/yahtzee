﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Yahtzee;
using System.Net.Sockets;

namespace YahtzeeTests
{
	[TestClass]
	public class GameServerTester
	{
		const int baseBindAddress = 26513;
		[TestMethod]
		public void BindAddress()
		{
			TcpClient client = new TcpClient("127.0.0.1", createServer());
			if (!client.Connected)
				Assert.Fail("can not connect to server");
		}

		[TestMethod]
		public void RemoteRoll()
		{
			TcpClient client = new TcpClient("127.0.0.1", createServer());
			if (!client.Connected)
				Assert.Fail("can not connect to server");
			var sendStream = new System.IO.StreamWriter(client.GetStream());
			var recvStream = new System.IO.StreamReader(client.GetStream());
			sendStream.WriteLine("r_roll");
			sendStream.Flush();
			string resp = recvStream.ReadLine();
			int roll = int.Parse(resp);
			if (roll < 1 || roll > 6)
				Assert.Fail("bad roll");
		}

		[TestMethod]
		public void RemoteScore()
		{
			TcpClient client = new TcpClient("127.0.0.1", createServer());
			if (!client.Connected)
				Assert.Fail("can not connect to server");
			var sendStream = new System.IO.StreamWriter(client.GetStream());
			var recvStream = new System.IO.StreamReader(client.GetStream());
			sendStream.WriteLine("r_score 1 2 3 4 5 " + ScoreSlot.Straight_l.ToString());
			sendStream.Flush();
			int score = int.Parse(recvStream.ReadLine());
			Assert.AreEqual(40, score);
		}

		[TestMethod]
		public void remotPlayerRoll()
		{
			TcpClient client = new TcpClient("127.0.0.1", createServer());
			if (!client.Connected)
				Assert.Fail("can not connect to server");
			var sendStream = new System.IO.StreamWriter(client.GetStream());
			var recvStream = new System.IO.StreamReader(client.GetStream());
			sendStream.WriteLine("p_roll");
			sendStream.Flush();
			int [] roll = recvStream.ReadLine().Split(' ').Select((s) => int.Parse(s)).ToArray();
			if (roll.Length != 5)
				Assert.Fail("bad roll");
			for (int i =0; i < 5; i++)
				if (roll[i] < 1 || roll[i] > 6)
					Assert.Fail("bad roll");
		}

		[TestMethod]
		public void remotePlayerReroll()
		{
			TcpClient client = new TcpClient("127.0.0.1", createServer());
			if (!client.Connected)
				Assert.Fail("can not connect to server");
			var sendStream = new System.IO.StreamWriter(client.GetStream());
			var recvStream = new System.IO.StreamReader(client.GetStream());
			sendStream.WriteLine("p_roll");
			sendStream.Flush();
			sendStream.WriteLine("p_reroll 1 0 1 0 1");
			sendStream.Flush();
			int[] roll1 = recvStream.ReadLine().Split(' ').Select((s) => int.Parse(s)).ToArray();
			int[] roll2 = recvStream.ReadLine().Split(' ').Select((s) => int.Parse(s)).ToArray();
			if (roll1.Length != 5 || roll2.Length != 5)
				Assert.Fail("bad reroll");
			for (int i = 0; i < 5; i++)
				if (roll1[i] < 1 || roll1[i] > 6 || roll2[i] < 1 || roll2[i] > 6)
					Assert.Fail("bad reroll");
			if (roll1[0] != roll2[0] ||
				roll1[0] != roll2[0] ||
				roll1[0] != roll2[0])
				Assert.Fail("reroll didn't keep old values");
		}

		[TestMethod]
		public void remotePlayerScore()
		{
			TcpClient client = new TcpClient("127.0.0.1", createServer());
			if (!client.Connected)
				Assert.Fail("can not connect to server");
			var sendStream = new System.IO.StreamWriter(client.GetStream());
			var recvStream = new System.IO.StreamReader(client.GetStream());
			sendStream.WriteLine("p_roll");
			sendStream.Flush();
			sendStream.WriteLine("p_mark " + ScoreSlot.Sum_1.ToString());
			sendStream.Flush();
			int[] roll = recvStream.ReadLine().Split(' ').Select((s) => int.Parse(s)).ToArray();
			string resp = recvStream.ReadLine();
			int score = int.Parse(resp);
			Assert.AreEqual(roll.Where((s) => s == 1).Sum(), score);
		}

		public static int createServer()
		{
			int bindAddress = baseBindAddress;
			GameServer gs;
			int searchRange = 50;
			for (int i = 0; i < searchRange; i++)
			{
				try
				{
					gs = new GameServer(bindAddress);
					break;
				}
				catch (Exception)
				{
					bindAddress++;
				}
			}
			if (bindAddress == baseBindAddress + searchRange)
				Assert.Fail("can not create server");
			return bindAddress;
		}
	}
}
