﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Yahtzee;
using System.Net.Sockets;

namespace YahtzeeTests
{
	[TestClass]
	public class RemotePlayerTester
	{
		[TestMethod]
		public void checkConstructor()
		{
			RemotePlayer player = createPlayer();
		}
		
		[TestMethod]
		public void checkReroll()
		{
			RemotePlayer player = createPlayer();
			player.Roll();
			var roll = player.GetRoll();
			roll.Wait();
			var oldArr = roll.Result;
			player.Reroll(new bool[] { false, false, true, true, false });
			roll = player.GetRoll();
			roll.Wait();
			var newArr = roll.Result;
			Assert.AreEqual(oldArr[2], newArr[2], "testing if reroll kept numbers - t1");
			Assert.AreEqual(oldArr[3], newArr[3], "testing if reroll kept numbers - t2");
			oldArr = roll.Result;
			player.Reroll(new bool[] { false, true, false, true, true });
			roll = player.GetRoll();
			roll.Wait();
			Assert.AreEqual(oldArr[1], newArr[1], "testing if reroll kept numbers - t3");
			Assert.AreEqual(oldArr[3], newArr[3], "testing if reroll kept numbers - t4");
			Assert.AreEqual(oldArr[4], newArr[4], "testing if reroll kept numbers - t5");
		}

		[TestMethod]
		public void checkRerollCount()
		{
			RemotePlayer player = createPlayer();
			player.Roll();
			player.Reroll(new bool[] { false, false, true, true, false });
			player.Reroll(new bool[] { false, true, false, true, true });
			try
			{
				player.Reroll(new bool[] { false, false, false, false, false });
				Assert.Fail("excceeded number of rerolls");
			}
			catch (Exception)
			{ }
		}

		[TestMethod]
		public void markScoreBeforeRoll()
		{
			RemotePlayer player = createPlayer();
			try
			{
				player.MarkScore(ScoreSlot.Chance);
				Assert.Fail("MarkScore without a roll");
			}
			catch (Exception)
			{ }
		}

		[TestMethod]
		public void markScore()
		{
			LocalRule judge = new LocalRule();
			int expectedScore = 0;
			RemotePlayer player = createPlayer();
			ScoreSlot[] slots = (ScoreSlot[])Enum.GetValues(typeof(ScoreSlot));

			for (var i = 0; i < slots.Length; i++)
			{
				player.Roll();
				if (i % 2 == 0)
					player.Reroll(new bool[] { true, false, i % 4 == 0, i % 5 == 0, i % 6 != 0 });
				if (i % 3 == 0)
					player.Reroll(new bool[] { false, true, i % 4 == 0, i % 5 == 0, i % 6 != 0 });
				var roll = player.GetRoll();
				roll.Wait();
				judge.Score(roll.Result, slots[i]).ContinueWith((s) => { expectedScore += s.Result; });
				player.MarkScore(slots[i]);
			}
			var scoreTask = player.GetScore();
			scoreTask.Wait();
			Assert.AreEqual(expectedScore, scoreTask.Result);
		}

		private RemotePlayer createPlayer()
		{
			TcpClient client = new TcpClient("127.0.0.1", GameServerTester.createServer());
			return new RemotePlayer(client);
		}
	}
}
