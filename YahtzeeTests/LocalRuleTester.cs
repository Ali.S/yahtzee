﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Yahtzee;

namespace YahtzeeTests
{
	[TestClass]
	public class LocalRuleTester
	{
		const int RollCount = 100000;
		int[][] predefinedRollGroups = new int[][]{
			new int[]{5, 3, 2, 1, 6},
			new int[]{5, 5, 1, 4, 6},
			new int[]{1, 5, 6, 6, 2},
			new int[]{1, 3, 6, 1, 5},
			new int[]{2, 4, 3, 5, 2},
			new int[]{4, 3, 2, 6, 6},
			new int[]{5, 6, 5, 1, 4},
			new int[]{5, 5, 5, 4, 4},
			new int[]{6, 2, 1, 1, 1},
			new int[]{6, 4, 2, 5, 5},
			new int[]{5, 1, 3, 4, 4},
			new int[]{6, 5, 5, 6, 4},
			new int[]{1, 3, 5, 4, 2},
			new int[]{4, 6, 6, 4, 4},
			new int[]{5, 4, 4, 2, 4},
			new int[]{4, 4, 5, 4, 6},
			new int[]{4, 1, 3, 2, 1},
			new int[]{5, 5, 4, 2, 6},
			new int[]{1, 1, 1, 4, 4},
			new int[]{6, 2, 2, 2, 5},
			new int[]{3, 3, 3, 3, 3},
			new int[]{3, 2, 3, 3, 3},
		};

		[TestMethod]
		public void TestDiceGeneratorValidity()
		{
			LocalRule lr = new LocalRule();
			int failed = 0;
			for (int i = 0; i < RollCount; i++)
			{
				var task = lr.Roll();
				task.Wait();
				var roll = task.Result;
				if (roll < 1 || roll > 6)
					failed++;
			}

			Assert.IsTrue(failed == 0, String.Format("Roll should be between 1 and 6, rolled {0} times out of range", failed));
		}

		[TestMethod]
		public void TestDiceGeneratorEvenDitribution()
		{
			LocalRule lr = new LocalRule();
			int[] occurance = new int[] { 0, 0, 0, 0, 0, 0 };
			for (int i = 0; i < RollCount; i++)
			{
				var task = lr.Roll();
				task.Wait();
				var roll = task.Result;
				occurance[roll - 1]++;
			}
			int min = RollCount;
			int max = 0;
			for (int i = 0; i < occurance.Length; i++)
			{
				if (min > occurance[i])
					min = occurance[i];
				if (max < occurance[i])
					max = occurance[i];
			}
			double diff = max - min;
			diff /= RollCount / 6;
			Assert.AreEqual(0, diff, 0.05, String.Format("The roll chance is not evenly distributed {0} -> {1}", min, max));
		}

		[TestMethod]
		public void TestScoringChecks()
		{
			LocalRule lr = new LocalRule();
			foreach (ScoreSlot slot in Enum.GetValues(typeof(ScoreSlot)))
			{
				try
				{
					var scoreTask = lr.Score(new int[4], slot);
					scoreTask.Wait();
					Assert.Fail("score method is not failing with fewer than 5 roll values");
				}
				catch (Exception)
				{
				}
				try
				{
					var scoreTask = lr.Score(new int[6], slot);
					scoreTask.Wait();
					Assert.Fail("score method is not failing with fewer than 5 roll values");
				}
				catch (Exception)
				{
				}
				try
				{
					var scoreTask = lr.Score(new int[] { 1, 2, 3, 4, 7 }, slot);
					scoreTask.Wait();
					Assert.Fail("score method is not failing with invalid roll values");
				}
				catch (Exception)
				{
				}
			}
		}

		[TestMethod]
		public void TestScoreSum1()
		{
			LocalRule lr = new LocalRule();
			int[] expected = new int[] { 1, 1, 1, 2, 0, 0, 1, 0, 3, 0, 1, 0, 1, 0, 0, 0, 2, 0, 3, 0, 0, 0 };
			for (var i = 0; i < predefinedRollGroups.Length; i++)
			{
				var task = lr.Score(predefinedRollGroups[i], ScoreSlot.Sum_1);
				task.Wait();
				Assert.AreEqual(expected[i], task.Result);
			}
		}

		[TestMethod]
		public void TestScoreSum2()
		{
			LocalRule lr = new LocalRule();
			int[] expected = new int[] { 2, 0, 2, 0, 4, 2, 0, 0, 2, 2, 0, 0, 2, 0, 2, 0, 2, 2, 0, 6, 0, 2 };
			for (var i = 0; i < predefinedRollGroups.Length; i++)
			{
				var task = lr.Score(predefinedRollGroups[i], ScoreSlot.Sum_2);
				task.Wait();
				Assert.AreEqual(expected[i], task.Result);
			}
		}

		[TestMethod]
		public void TestScoreSum3()
		{
			LocalRule lr = new LocalRule();
			int[] expected = new int[] { 3, 0, 0, 3, 3, 3, 0, 0, 0, 0, 3, 0, 3, 0, 0, 0, 3, 0, 0, 0, 15, 12 };
			for (var i = 0; i < predefinedRollGroups.Length; i++)
			{
				var task = lr.Score(predefinedRollGroups[i], ScoreSlot.Sum_3);
				task.Wait();
				Assert.AreEqual(expected[i], task.Result);
			}
		}

		[TestMethod]
		public void TestScoreSum4()
		{
			LocalRule lr = new LocalRule();
			int[] expected = new int[] { 0, 4, 0, 0, 4, 4, 4, 8, 0, 4, 8, 4, 4, 12, 12, 12, 4, 4, 8, 0, 0, 0 };
			for (var i = 0; i < predefinedRollGroups.Length; i++)
			{
				var task = lr.Score(predefinedRollGroups[i], ScoreSlot.Sum_4);
				task.Wait();
				Assert.AreEqual(expected[i], task.Result);
			}
		}

		[TestMethod]
		public void TestScoreSum5()
		{
			LocalRule lr = new LocalRule();
			int[] expected = new int[] { 5, 10, 5, 5, 5, 0, 10, 15, 0, 10, 5, 10, 5, 0, 5, 5, 0, 10, 0, 5, 0, 0 };
			for (var i = 0; i < predefinedRollGroups.Length; i++)
			{
				var task = lr.Score(predefinedRollGroups[i], ScoreSlot.Sum_5);
				task.Wait();
				Assert.AreEqual(expected[i], task.Result);
			}
		}

		[TestMethod]
		public void TestScoreSum6()
		{
			LocalRule lr = new LocalRule();
			int[] expected = new int[] { 6, 6, 12, 6, 0, 12, 6, 0, 6, 6, 0, 12, 0, 12, 0, 6, 0, 6, 0, 6, 0, 0 };
			for (var i = 0; i < predefinedRollGroups.Length; i++)
			{
				var task = lr.Score(predefinedRollGroups[i], ScoreSlot.Sum_6);
				task.Wait();
				Assert.AreEqual(expected[i], task.Result);
			}
		}

		[TestMethod]
		public void TestScoreThrees()
		{
			LocalRule lr = new LocalRule();
			int[] expected = new int[] { 0, 0, 0, 0, 0, 0, 0, 23, 11, 0, 0, 0, 0, 24, 19, 23, 0, 0, 11, 17, 15, 14 };
			for (var i = 0; i < predefinedRollGroups.Length; i++)
			{
				var task = lr.Score(predefinedRollGroups[i], ScoreSlot.ThreeOfKind);
				task.Wait();
				Assert.AreEqual(expected[i], task.Result);
			}
		}

		[TestMethod]
		public void TestScoreFours()
		{
			LocalRule lr = new LocalRule();
			int[] expected = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 15, 14 };
			for (var i = 0; i < predefinedRollGroups.Length; i++)
			{
				var task = lr.Score(predefinedRollGroups[i], ScoreSlot.FourOfKind);
				task.Wait();
				Assert.AreEqual(expected[i], task.Result);
			}
		}

		[TestMethod]
		public void TestScoreFullHouse()
		{
			LocalRule lr = new LocalRule();
			int[] expected = new int[] { 0, 0, 0, 0, 0, 0, 0, 25, 0, 0, 0, 0, 0, 25, 0, 0, 0, 0, 25, 0, 25, 0 };
			for (var i = 0; i < predefinedRollGroups.Length; i++)
			{
				var task = lr.Score(predefinedRollGroups[i], ScoreSlot.FullHouse);
				task.Wait();
				Assert.AreEqual(expected[i], task.Result);
			}
		}

		[TestMethod]
		public void TestScoreStraight_s()
		{
			LocalRule lr = new LocalRule();
			int[] expected = new int[] { 0, 0, 0, 0, 30, 0, 0, 0, 0, 0, 0, 0, 30, 0, 0, 0, 30, 0, 0, 0, 0, 0 };
			for (var i = 0; i < predefinedRollGroups.Length; i++)
			{
				var task = lr.Score(predefinedRollGroups[i], ScoreSlot.Straight_s);
				task.Wait();
				Assert.AreEqual(expected[i], task.Result);
			}
		}

		[TestMethod]
		public void TestScoreStraight_l()
		{
			LocalRule lr = new LocalRule();
			int[] expected = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 40, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
			for (var i = 0; i < predefinedRollGroups.Length; i++)
			{
				var task = lr.Score(predefinedRollGroups[i], ScoreSlot.Straight_l);
				task.Wait();
				Assert.AreEqual(expected[i], task.Result);
			}
		}

		[TestMethod]
		public void TestScoreYahtzee()
		{
			LocalRule lr = new LocalRule();
			int[] expected = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 50, 0 };
			for (var i = 0; i < predefinedRollGroups.Length; i++)
			{
				var task = lr.Score(predefinedRollGroups[i], ScoreSlot.Yahtzee);
				task.Wait();
				Assert.AreEqual(expected[i], task.Result);
			}
		}

		[TestMethod]
		public void TestScoreChance()
		{
			LocalRule lr = new LocalRule();
			int[] expected = new int[] { 17, 21, 20, 16, 16, 21, 21, 23, 11, 22, 17, 26, 15, 24, 19, 23, 11, 22, 11, 17, 15, 14 };
			for (var i = 0; i < predefinedRollGroups.Length; i++)
			{
				var task = lr.Score(predefinedRollGroups[i], ScoreSlot.Chance);
				task.Wait();
				Assert.AreEqual(expected[i], task.Result);
			}
		}
	}
}
