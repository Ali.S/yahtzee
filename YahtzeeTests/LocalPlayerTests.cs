﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Yahtzee;

namespace YahtzeeTests
{
	[TestClass]
	public class LocalPlayerTester
	{
		class LocalPlayerTesterRuleset : LocalRule
		{
			public int RollCalls { get; private set; }
			public int ScoreCalls { get; private set; }
			public Queue<int> DieInejection { get; private set; }
			public LocalPlayerTesterRuleset()
			{
				DieInejection = new Queue<int>();
				reset();
			}
			public void reset()
			{
				RollCalls = 0;
				ScoreCalls = 0;
			}
			public override Task<int> Roll()
			{
				RollCalls++;
				if (DieInejection.Count > 0)
				{
					int ret = DieInejection.Dequeue();
					return Task.Run(() => ret);
				}
				else
					return base.Roll();
			}

			public override Task<int> Score(int[] roll, ScoreSlot slot)
			{
				ScoreCalls++;
				return base.Score(roll, slot);
			}
		};
		[TestMethod]
		public void checkConstructor()
		{
			LocalPlayerTesterRuleset rule = new LocalPlayerTesterRuleset();
			LocalPlayer player = new LocalPlayer(rule);
		}

		[TestMethod]
		public void checkCallForRoll()
		{
			LocalPlayerTesterRuleset rule = new LocalPlayerTesterRuleset();
			LocalPlayer player = new LocalPlayer(rule);
			player.Roll();
			var roll = player.GetRoll();
			roll.Wait();
			Assert.AreEqual(5, rule.RollCalls);
		}
		[TestMethod]
		public void checkCallForReroll()
		{
			LocalPlayerTesterRuleset rule = new LocalPlayerTesterRuleset();
			LocalPlayer player = new LocalPlayer(rule);
			player.Roll();
			var roll = player.GetRoll();
			roll.Wait();
			var oldArr = roll.Result;
			player.Reroll(new bool[] { false, false, true, true, false });
			roll = player.GetRoll();
			roll.Wait();
			var newArr = roll.Result;
			Assert.AreEqual(8, rule.RollCalls);
			Assert.AreEqual(oldArr[2], newArr[2], "testing if reroll kept numbers - t1");
			Assert.AreEqual(oldArr[3], newArr[3], "testing if reroll kept numbers - t2");
			oldArr = roll.Result;
			player.Reroll(new bool[] { false, true, false, true, true });
			roll = player.GetRoll();
			roll.Wait();
			Assert.AreEqual(10, rule.RollCalls);
			Assert.AreEqual(oldArr[1], newArr[1], "testing if reroll kept numbers - t3");
			Assert.AreEqual(oldArr[3], newArr[3], "testing if reroll kept numbers - t4");
			Assert.AreEqual(oldArr[4], newArr[4], "testing if reroll kept numbers - t5");
		}
		[TestMethod]
		public void checkRerollCount()
		{
			LocalPlayerTesterRuleset rule = new LocalPlayerTesterRuleset();
			LocalPlayer player = new LocalPlayer(rule);
			player.Roll();
			var roll = player.GetRoll();
			player.Reroll(new bool[] { false, false, true, true, false });
			player.Reroll(new bool[] { false, true, false, true, true });
			try
			{
				player.Reroll(new bool[] { false, false, false, false, false });
				Assert.Fail("excceeded number of rerolls");
			}
			catch (Exception)
			{ }
		}
		[TestMethod]
		public void markScoreBeforeRoll()
		{
			LocalPlayerTesterRuleset rule = new LocalPlayerTesterRuleset();
			LocalPlayer player = new LocalPlayer(rule);
			try
			{
				player.MarkScore(ScoreSlot.Chance);
				Assert.Fail("MarkScore without a roll");
			}
			catch (Exception)
			{ }
		}

		[TestMethod]
		public void markScore()
		{
			LocalRule judge = new LocalRule();
			int expectedScore = 0;
			LocalPlayerTesterRuleset rule = new LocalPlayerTesterRuleset();
			LocalPlayer player = new LocalPlayer(rule);
			ScoreSlot[] slots = (ScoreSlot[])Enum.GetValues(typeof(ScoreSlot));

			for (var i = 0; i < slots.Length; i++)
			{
				player.Roll();
				if (i % 2 == 0)
					player.Reroll(new bool[] { true, false, i % 4 == 0, i % 5 == 0, i % 6 != 0 });
				if (i % 3 == 0)
					player.Reroll(new bool[] { false, true, i % 4 == 0, i % 5 == 0, i % 6 != 0 });
				var roll = player.GetRoll();
				roll.Wait();
				judge.Score(roll.Result, slots[i]).ContinueWith((s) => { expectedScore += s.Result; });
				player.MarkScore(slots[i]);
			}
			var scoreTask = player.GetScore();
			scoreTask.Wait();
			if (rule.ScoreCalls < 13)
				Assert.Fail("Not enough calls to score");
			Assert.AreEqual(expectedScore, scoreTask.Result);
		}

		[TestMethod]
		public void additionalYahtzee()
		{
			LocalPlayerTesterRuleset rule = new LocalPlayerTesterRuleset();
			for (int i=0; i < 15; i++)
				rule.DieInejection.Enqueue(1);
			LocalPlayer player = new LocalPlayer(rule);
			player.Roll();
			player.MarkScore(ScoreSlot.Yahtzee);
			player.Roll();
			try
			{
				player.MarkScore(ScoreSlot.Yahtzee);
				Assert.Fail("second yahtzee is scored automatically");
			}
			catch (Exception)
			{ }
			try
			{
				player.MarkScore(ScoreSlot.Sum_2);
				Assert.Fail("second yahtzee should mark correct slot");
			}
			catch (Exception)
			{ }
			player.MarkScore(ScoreSlot.Sum_1);
			var scoreTask = player.GetScore();
			Assert.AreEqual(105, scoreTask.Result);
		}

	}
}
